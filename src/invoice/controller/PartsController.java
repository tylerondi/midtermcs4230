package invoice.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import invoice.model.AuthenticationModel;
import invoice.model.PartBean;
import invoice.model.PartModel;

import java.io.PrintWriter;

public class PartsController extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, 
						HttpServletResponse response) 
						throws ServletException, IOException {
		ArrayList<PartBean> parts = PartModel.getParts();
		
		request.setAttribute("parts", parts);
		getServletContext().getRequestDispatcher("/partmanagement.jsp").forward(request, response);
	}
	
}
