package invoice.model;

public class PartBean implements java.io.Serializable {

	private static final long serialVersionUID = 3636102615259283578L;

	private int id;
	private String partNumber;
	private String description;
	private long cost;
	
	public PartBean() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCost() {
		return cost;
	}

	public void setCost(long cost) {
		this.cost = cost;
	}
}
