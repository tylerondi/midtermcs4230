package invoice.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PartModel {
	public static ArrayList<PartBean> getParts() {
		ArrayList<PartBean> parts = new ArrayList<PartBean>();
		Connection connection = DatabaseConnectionFactory.getInstance().getDatabaseConnection();
		PreparedStatement statement = null;
		if (connection != null) {
			try {
				statement = connection.prepareStatement("SELECT * " +
														"FROM part p ");
				
				ResultSet rs = statement.executeQuery();
				if (rs != null && !rs.isClosed() && rs.next()) {
					do{
						PartBean part = new PartBean();
						part.setId(rs.getInt("id"));
						part.setDescription(rs.getString("description"));
						part.setPartNumber(rs.getString("part_number"));
						part.setCost(rs.getLong("cost"));
						parts.add(part);
					}while(rs.next());
				}
			} catch (SQLException e) {
				System.err.println("Error retrieving hash for comparison");
				e.printStackTrace();
			}
		}
		
		return parts;
	}
}
