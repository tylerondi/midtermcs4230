package invoice.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import invoice.model.AuthenticationModel;
import invoice.model.CustomerBean;
import invoice.model.CustomerModel;
import invoice.model.PartBean;
import invoice.model.PartModel;

import java.io.PrintWriter;

public class CustomerController extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, 
						HttpServletResponse response) 
						throws ServletException, IOException {
		ArrayList<CustomerBean> customers = CustomerModel.getCusotmers();
		
		request.setAttribute("customers", customers);
		getServletContext().getRequestDispatcher("/customermanagement.jsp").forward(request, response);
	}
	
}
