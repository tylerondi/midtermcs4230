package invoice.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CustomerModel {
	public static ArrayList<CustomerBean> getCusotmers() {
		ArrayList<CustomerBean> customers = new ArrayList<CustomerBean>();
		Connection connection = DatabaseConnectionFactory.getInstance().getDatabaseConnection();
		PreparedStatement statement = null;
		if (connection != null) {
			try {
				statement = connection.prepareStatement("SELECT * " +
														"FROM customer c ");
				
				ResultSet rs = statement.executeQuery();
				if (rs != null && !rs.isClosed() && rs.next()) {
					do{
						CustomerBean customer = new CustomerBean();
						customer.setId(rs.getInt("id"));
						customer.setName(rs.getString("name"));
						customer.setAddress(rs.getString("address"));
						customer.setBalance(rs.getLong("balance"));
						customers.add(customer);
					}while(rs.next());
				}
			} catch (SQLException e) {
				System.err.println("Error retrieving hash for comparison");
				e.printStackTrace();
			}
		}
		
		return customers;
	}
}
