package invoice.model;

public class CustomerBean implements java.io.Serializable {

	private static final long serialVersionUID = 7870948171204604923L;
	
	private int id;
	private String name;
	private String address;
	private long balance;
	
	public CustomerBean() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}
}
